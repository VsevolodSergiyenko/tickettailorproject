package Pages

import geb.Page

class LoginPage extends Page {
    /**
     * "At checker" that the browser uses for checking if it is pointing at a given page
     */
    static at = { driver.currentUrl.contains("login") }
    /**
     * General PageObject specification module
     */
    static content = {
        loginForm { $(".ticket_tailor_sign") }
        emailAddressField { $("#username") }
        passwordField { $("#password") }
        loginButton(to: UserHomePage) { $("#submit") }
        loginErrorMessage { $ { ".error" } }

        loginErrorMessageText { loginErrorMessage.text() }
    }

    /**
     * Login to application with valid credentials
     * @param email
     * @param password
     * @return
     */
    UserHomePage loginToUserAccount(String email, String password) {
        emailAddressField.value(email)
        passwordField.value(password)
        loginButton.click()
        return browser.page
    }

}