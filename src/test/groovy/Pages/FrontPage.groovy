package Pages

import geb.Page

/**
 */
class FrontPage extends Page {

    /**
     * Page URL definition used when using the driver "to()" method
     */
    static url = "http://staging.tickettailor.com/"
    /**
     * "At checker" that the browser uses for checking if it is pointing at a given page
     */
    static at = { title.contains("Sell tickets online YOUR way!") }
    /**
     * General PageObject specification module
     */
    static content = {
        loginLink(to: LoginPage) { $("li.login>a") }
    }

    /**
     * Go to login page
     * @return
     */
    LoginPage goToLoginPage(){
        loginLink.click()
        return browser.page
    }

}
