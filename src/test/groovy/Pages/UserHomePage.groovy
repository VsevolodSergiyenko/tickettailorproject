package Pages

import geb.Page

class UserHomePage extends Page {
    static at = { title == "Ticket Tailor / Dashboard" }
    static content = {
        dashboardTile { $("#menu-dashboard") }
    }

    /**
     * Wait for page load
     * @return
     */
    UserHomePage waitForDashboardTile() {
        waitFor { dashboardTile }
        return browser.page
    }

}
