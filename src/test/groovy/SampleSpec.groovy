import Pages.FrontPage
import Pages.LoginPage
import Pages.UserHomePage
import geb.spock.GebReportingSpec
import spock.lang.Stepwise

@Stepwise
class SampleSpec extends GebReportingSpec {
    private static final String CORRECT_EMAIL = "sergiyenko.ssita@gmail.com";
    private static final String CORRECT_PASSWORD = "111111";

    def "Login to application: Positive test(correct email and password)"() {
        given:"Go to the ticket tailor front page"
        to FrontPage

        when:"I'm at ticket tailor front page"
        at FrontPage

        then:"I go to login page"
        waitFor { loginLink }
        goToLoginPage()

        when:"I'm at login page"
        waitFor { LoginPage }
        at LoginPage

        then:"I login to application with valid credentials"
        waitFor { loginForm }
        loginToUserAccount(CORRECT_EMAIL,CORRECT_PASSWORD)
        at UserHomePage
        waitForDashboardTile()
        browser.clearCookies()
    }

}
